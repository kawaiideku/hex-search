# Find occurences of hex in file

Name the file you want to search `boot.dat` in the directory. (You can also
modify the program to open the file which you provide as input. I'm just lazy
😅)

Now run the following command to count the occurrences

	./a.out | grep -o <hex to search for> | wc -l

- `-o` option for grep is needed to match multiple occurrences in a line
- `-l` option for wc is needed to count the lines

