#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main() {
  FILE *fp;
  int i;
  fp = fopen("boot.dat", "rb");
  if (!fp) {
    printf("Couldn't read file.. exiting");
    return 1;
  }

  // to get number of characters
  fseek(fp, 0, SEEK_END);
  int size = ftell(fp);
  rewind(fp);

  // buffer for holding all data read
  char *buffer = (char *)malloc(sizeof(char) * size);
  if (!buffer) {
    printf("Failed to malloc\n");
    return 1;
  }

  // read & print all contents
  fread(buffer, 1, size, fp);
  for (i = 0; i < size; i++) {
    printf("%x", buffer[i]);
  }

  fclose(fp);
  free(buffer);
}
